#!/bin/bash
#
# Script post installation d'Oracle 11gR2
# Lotaut Brian aka LBTM - 12/2011
# Script libre: GPLv3
#
# Syntaxe: # sudo ./oracle11gr2postinstall.sh
script_version="0.2"


# Test que le script est lance en root
if [ "$(id -u)" != "0" ]; then 
	echo "Le script doit etre execute en tant que root."
  	echo "Syntaxe: su - ./oracle11gr2postinstall.sh"
  	exit 1
fi
		

# Fonctions
defvarenv(){
echo "Veuillez entrer 'ORACLE_HOSTNAME' (fqdn):"
	read ORACLE_HOSTNAME
	
echo "Veuillez entrer 'ORACLE_UNQNAME' (openview):"
	read ORACLE_UNQNAME
	
echo "Veuillez entrer 'ORACLE_BASE' (/opt/oracle):"
	read ORACLE_BASE
	
echo "Veuillez entrer 'ORACLE_HOME' (/opt/oracle/product/11.2.0/dbhome):"
	read ORACLE_HOME

echo "Veuillez entrer 'ORACLE_SID' (openview):"
	read ORACLE_SID

echo "Veuillez entrer 'ORACLE_OWNER' (oracle):"
	read ORACLE_OWNER
	
echo "------------------------------------------------"
echo "Liste des informations entre"
echo "------------------------------------------------"
echo "ORACLE_HOSTNAME: $ORACLE_HOSTNAME"
echo "ORACLE_UNQNAME: $ORACLE_UNQNAME"
echo "ORACLE_BASE: $ORACLE_BASE"
echo "ORACLE_HOME: $ORACLE_HOME"
echo "ORACLE_SID: $ORACLE_SID"
echo "ORACLE_OWNER: $ORACLE_OWNER"
echo "------------------------------------------------"

echo "Les informations entre sont elles correcte ? (o/n)"
	read reponse
	
	if [ $reponse == "o" ]; then
		echo "----------------------------------------------------"
		echo "Export des variables d'environnement et PATH"
		echo "----------------------------------------------------"
		exportvarenv
	else 
		defvarenv
	fi
}

listner(){
	su - $ORACLE_OWNER -c "$ORACLE_HOME/bin/lsnrctl start"
echo "Le service LISTNER a ete demarre"
}

startdatabase(){
	su - $ORACLE_OWNER -c "$ORACLE_HOME/bin/dbstart $ORACLE_HOME"
echo "La base de donnée a ete demarre"
}

startoraclemanager(){
	su - $ORACLE_OWNER -c "$ORACLE_HOME/bin/emctl start dbconsole"
echo "Oracle Enterprise Manager demarre"
echo "Page Web: https://$ORACLE_HOSTNAME:1158/em/console/logon"
}

exportvarenv(){
echo "#----------------" >> /etc/profile
echo "#Oracle Settings." >> /etc/profile
echo "#----------------" >> /etc/profile
echo "TMP=/tmp; export TMP" >> /etc/profile
echo "TMPDIR=$TMP; export TMPDIR" >> /etc/profile
echo "ORACLE_HOSTNAME=$ORACLE_HOSTNAME; export ORACLE_HOSTNAME" >> /etc/profile
echo "ORACLE_UNQNAME=$ORACLE_UNQNAME; export ORACLE_UNQNAME" >> /etc/profile
echo "ORACLE_BASE=$ORACLE_BASE; export ORACLE_BASE" >> /etc/profile
echo "ORACLE_HOME=$ORACLE_HOME; export ORACLE_HOME" >> /etc/profile
echo "ORACLE_SID=$ORACLE_SID; export ORACLE_SID" >> /etc/profile
echo "ORACLE_TERM=hp; export ORACLE_TERM" >> /etc/profile
echo "PATH=/usr/sbin:$PATH; export PATH" >> /etc/profile
echo "PATH=$ORACLE_HOME/bin:$PATH; export PATH" >> /etc/profile
echo "LD_LIBRARY_PATH=$ORACLE_HOME/lib:/lib:/usr/lib; export LD_LIBRARY_PATH" >> /etc/profile 
echo "CLASSPATH=$ORACLE_HOME/jre:$ORACLE_HOME/jlib:$ORACLE_HOME/rdbms/jlib; export CLASSPATH" >> /etc/profile
echo "if [ $USER = "oracle" ]; then" >> /etc/profile
echo "	if [ $SHELL = "/bin/ksh" ]; then" >> /etc/profile
echo "		ulimit -p 16384" >> /etc/profile
echo "		ulimit -n 65536" >> /etc/profile
echo "	else" >> /etc/profile
echo "		ulimit -u 16384 -n 65536" >> /etc/profile
echo "	fi" >> /etc/profile
echo "fi" >> /etc/profile
echo "#----------------" >> /etc/profile
echo "Terminer"

echo "Activer les variables positionnees precedemment sous la session courante"
	source /etc/profile
echo "Terminer"
}

firewall(){
echo "Ajout des ports (1521 et 1158)"
	/sbin/iptables -A INPUT -p tcp --dport 1521 -j ACCEPT
	/sbin/iptables -A INPUT -p tcp --dport 1158 -j ACCEPT
echo "Terminer"
}


# Execution du programme
echo "----------------------------------------------------"
echo "Definition des variables d'environnement d'Oracle"
echo "----------------------------------------------------"
defvarenv

echo "----------------------------------------------------"
echo "Definition des regles du firewall"
echo "----------------------------------------------------"
firewall

echo "----------------------------------------------------"
echo "Demarrage des services:"
echo "----------------------------------------------------"

echo "----"
echo "Demarrage LISTNER"
listner
echo "----"
echo "Demarrage de la base de donnee"
startdatabase
echo "----"
echo "Demarrage d'Oracle Enterprise Manager pour administrer la base depuis l'interface web"
startoraclemanager
echo "----"
echo "La base de donnée Oracle est operationnel" 
echo "----"
# Fin du script